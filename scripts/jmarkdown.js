$(function() {
    //忽略HTML代码
    if (document.doctype)
        return;
    // 初始化基本HTML
    initHtml();
    appendCss("scripts/jmarkdown.css");
    appendCss("scripts/highlight/styles/monokai.css");
    appendCss("scripts/katex/katex.min.css");
    // 初始化变量
    // Markdown文本容器
    var jTextContainer = $('#text-container');
    // Markdown预览容器
    var jMarkdownContainer = $('#markdown-container');
    // Markdown Outline 容器
    var jOutline = $('#markdown-outline');
    // 是否为Markdown模式
    var bPreviewMode = true;
    // 目录总数
    var lenOutline = 0;

    jOutline.hide();
    window.onresize = updateOutlineVisible;    
    // 双击切换
    $(document).on('dblclick',toggleMode);


    // ***********************
    // *       Functions     *
    // ***********************
    /**
     * 初始化基本HTML
     */
    function initHtml() {
        document.write('<!DOCTYPE html>');
        document.write('<html>');
        document.write('<head></head>');
        document.write('<body>');
        document.write('<div id="container" class="viewport-flip">');
        document.write('<div id="text-container" class="content flip" style="display:none;"></div>');
        document.write('<div id="markdown-container" class="content flip"></div>');
        document.write('</div>');
        document.write('<div id="markdown-outline"></div>');
        document.write('<div id="markdown-backTop" onclick="window.scrollTo(0,0);"></div>');
        document.write('</body></html>');
        document.close();    
    }

    /**
     * 切换预览与代码
     */
    function toggleMode(e){
        if ($(e.target).closest('.content').length != 0) return;
        // 内容区外点击才会切换模式        
        if(bPreviewMode){
            jMarkdownContainer.addClass('out').removeClass('in');
            bPreviewMode = false;
            jMarkdownContainer.one('webkitAnimationEnd', function() {
                jTextContainer.show().addClass('in').removeClass('out');
                jMarkdownContainer.hide();
            });
        } else {
            jTextContainer.addClass('out').removeClass('in');
            jTextContainer.one('webkitAnimationEnd', function(){
                jMarkdownContainer.show().addClass('in').removeClass('out');
                jTextContainer.hide();
                bPreviewMode = true;
            });
        }
        return false;
    }
    
    /**
     * 监视当前URL变化
     */
    var bLocal = isLocalURL(location.href);
    function updateURL() {
        $.get(location.href, function(data, status) {
            if (status) updateMarkdown(data);
        });
        if (bLocal) {
            setTimeout(updateURL, 1000);
        }
    }
    updateURL();

    /**
     * 更新页面上的Markdown
     * @param test Markdown格式文本
     */
    var lastText = null;
    /** Markdown 转换器 */
    var markdownConverter = new showdown.Converter({
        tables: true,
        strikethrough: true,
        simplifiedAutoLink: true,
        tasklists: true,
        literalMidWordUnderscores: true
    });
    jMarkdownContainer.on('webkitAnimationEnd', function(){
        updateOutlineVisible();
    });
    function updateMarkdown(text) {
        // 查看Markdown内容是否变化
        if(text == lastText) return;
        // Markdown 转换
        lastText = text;
        var sText = text.replace(/\r\n?|\r?\n/g, '\r\n');
        var sHtml = markdownConverter.makeHtml(lastText);
        jTextContainer.text(sText);
        jMarkdownContainer.html(sHtml);
        // 高亮代码区域
        $('pre code').each(function(i, block) {
            hljs.highlightBlock(block);
        }); 
        // 更新目录
        updateOutline();
    }

    /**
     * 根据当前页面更新Outline
     */
    function updateOutline() {
        var arrHeaders = document.querySelectorAll("h1,h2,h3,h4,h5,h6");
        var sOutline = ['<ul>'];
        // 变量，用来遍历所有Header
        var oHeader, sHeader, idHeader = 0;
        // 目录级别, 分别代表：当前级别，上个Header级别，与上个Header Level之间的级别差
        var iHeaderLevel = 0, iLastHeaderLevel = 1, iLevelCount = 0;
        // 遍历Headers
        for(var i = 0; i < arrHeaders.length; i ++) {
            oHeader = arrHeaders[i];
            sHeader = oHeader.innerText;
            // 为Header设置ID
            oHeader.setAttribute("id", idHeader);
            // 计算Level
            iHeaderLevel = oHeader.tagName.match(/^h(\d)$/i)[1]; // 当前level级别
            iLevelCount = iHeaderLevel - iLastHeaderLevel;
            // 根据级别差生成代码
            if (iLevelCount > 0) {
                // 比上个Header级别低，如h2低于h1
                // 根据级别差生成代码<ul>
                for (var j = 0; j < iLevelCount; j ++) {
                    sOutline.push('<ul>');
                }
            } else if(iLevelCount < 0) {
                // 比上个Header级别高，如h1高于h2
                // 根据级别差生成代码</ul>
                iLevelCount *= -1;
                for (var j = 0; j < iLevelCount; j++) {
                    sOutline.push('</ul>');
                }
            }
            // 插入一条目录
            sOutline.push('<li>');
            sOutline.push('<a href="#' + idHeader + '">' + sHeader + '</a>');
            sOutline.push('</li>');
            iLastHeaderLevel = iHeaderLevel;
            idHeader ++;
        }
        sOutline.push('</ul>')
        lenOutline = idHeader;
        if (idHeader > 0) {
            jOutline.html(sOutline.join(''));
            jOutline.find('ul').each(function(i, n) {
                var jThis = $(this);
                if(jThis.children('li').length == 0){
                    jThis.replaceWith(jThis.children());
                }
            });
        }
        updateOutlineVisible();
    }

    /**
     * 设置目录的可见性
     * @param 目录的可见性
     */
    function updateOutlineVisible() {
        if(!bPreviewMode) {
            jOutline.hide();
            return;
        }
        if (lenOutline <= 0) {
            jOutline.hide();
        }
        var offset = jMarkdownContainer.offset();
        jOutline.css({
            left: offset.left + jMarkdownContainer.outerWidth() + 10 + 'px',
            maxHeight: document.body.clientHeight - 30
        }).show();
    }
});
