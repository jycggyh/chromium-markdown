
/**
 * 向网页Head内增加css
 * @param url css文件的URL，将会把其作为chrome.extension.getURL(url)的参数
 *      获取真正的URL。
 */
function appendCss(url) {
    var oNode = document.createElement("link");
    oNode.rel = "stylesheet";
    oNode.href = chrome.extension.getURL(url);
    document.head.appendChild(oNode);
}

/**
 * 监视当前地址栏链接，如果该链接是个本地文件，则会跟踪其变化。
 * @param funName 回调函数
 * @param 本地文件的更新间隔
 */
function watchCurrentURL(funName, iTimeout) {
    watchURL(location.href, funName, iTimeout);
}

/**
 * 监视链接，如果该链接是个本地文件，则会跟踪其变化。
 * @param 要监视的URL。
 * @param funName 回调函数
 * @param 本地文件的更新间隔
 */
function watchURL(url, funName, iTimeout) {
    if(isLocalURL(url)) {
        setInterval(function() {
            $.get(url, funName);
        }, iTimeout);
    } else {
        $.get(url, funName);
    }
}

/**
 * 判断该URL是否是本地URL
 * @param url 要判断的URL
 * @return bool 是否是本地URL
 */
function isLocalURL(url) {
    return /^file:\/\//i.test(url);
}
