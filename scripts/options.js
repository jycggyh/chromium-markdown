// 显示信息的对话框
registerDialogEvent("#info-dialog", "#info-dialog-handle", "click");

/**
 * 注册对话框打开事件
 * @param dialogSelector 对话框选择器
 * @param handleSelector 触发事件的选择器
 * @param events 触发的事件
 */
function registerDialogEvent(dialogSelector, handleSelector, events) {
    var dialog = document.querySelector(dialogSelector);
    var handle = document.querySelector(handleSelector);

    if(!dialog.showModal) {
        dialogPolyfill.registerDialog(dialog);
    }

    handle.addEventListener(events, function() {
        dialog.showModal();
    });

    dialog.querySelector(".close").addEventListener("click", function() {
        dialog.close();
    });
}
