# JMarkdown
在Chromium内核的浏览器上读取并解析Markdown文件  
ps：本项目是fork自Google Play上的一个扩展，并在其基础上做了些许增强功能。 
## 特点
1. 解析Markdown文件
2. 当本地文件有变化时自动加载
3. 高亮代码区域
4. 在内容右侧显示目录
5. 支持扩展: Tasklist, table, LaTeX
6. 在内容区域外双击可以在代码模式和预览模式见切换

## 安装
当前最新版本：0.0.1.snapshot
- Google: 待发布
- Download: [JMarkdown](http://git.oschina.net/jycggyh/chromium-markdown/releases)

# 演示
## 列表
- [x] This task is done
- [X] This task is done too
- [ ] This is still pending

## 代码
    | Left-Aligned  | Center Aligned  | Right Aligned |
    | :------------ |:---------------:| -----:|
    | **col 3 is**      | some wordy text | $1600 |
    | ~~col 2 is~~      | centered        |   $12 |
    | zebra stripes | are neat        |    $1 |

```
function test() {
  console.log("notice the blank line before this function?");
}
```

```ruby
require 'redcarpet'
markdown = Redcarpet.new("Hello World!")
puts markdown.to_html
```

## 表格
| Left-Aligned  | Center Aligned  | Right Aligned |
| :------------ |:---------------:| -----:|
| **col 3 is**      | some wordy text | $1600 |
| ~~col 2 is~~      | centered        |   $12 |
| zebra stripes | are neat        |    $1 |

## LaTeX
Inline LaTeX: $$E=mc^2$$

Block LaTeX:

$$
E=mc^2
$$

Escaped LaTeX: \$$E=mc^2$$

# 感谢
感谢以下开源项目  

|   名称      |               网址                      |                          说明                       |
| :-----------| :-------------------------------------- | :-------------------------------------------------- |
| showdown.js | https://github.com/showdownjs/showdown  | A Markdown to HTML converter written in Javascript  |
| Katex       | https://github.com/Khan/KaTeX           | Fast math typesetting for the web                   |
| highlightjs | https://highlightjs.org/                | Syntax highlighting for the Web                     |




